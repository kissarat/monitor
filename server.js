var http = require('http');
var SocketServer = require('ws').Server;

var dialog = [];

var server = http.createServer(function(req, res) {
    res.end(JSON.stringify(dialog));
});

var clients = new Map();

var socketServer = new SocketServer({port: 5000});
socketServer.on('connection', function(socket) {
    socket.on('message', function(data) {
        var req = JSON.parse(data);

        switch (req.command) {
            case 'enter':
                if (clients.has(req.auth)) {
                    this.close();
                }
                else {
                    this.name = req.name || 'user' + (clients.size + 1);
                    this.id = req.auth;
                    clients.set(req.auth, this);
                }
                break;
            case 'exit':
                if (clients.has(this.id)) {
                    clients.remove(this.id)
                }
                break;
            default:
                clients.forEach(function(client) {
                    var res = {
                        author: client.name,
                        content: req.message
                    };
                    dialog.push(res);
                    if (client.OPEN == client.readyState) {
                        client.send(JSON.stringify(res));
                    }
                    else {
                        clients.delete(client.id);
                    }
                });
                break;
        }
    });
});

server.listen(3000);
