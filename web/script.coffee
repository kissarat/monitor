App = new Marionette.Application()
App.addRegions
  main: '#main'

App.module 'Chat', (Chat, App) ->
  class Chat.Replica extends Backbone.Model

  class Chat.Dialog extends Backbone.Collection
    model: Chat.Replica
    url: '/api/dialog'

  class Chat.ReplicaView extends Marionette.ItemView
    model: Chat.Replica
    template: '#replica-template'
    tagName: 'li'

  class Chat.DialogView extends Marionette.CompositeView
#    model: Backbone.Model()
    template: '#dialog-template'
    childView: Chat.ReplicaView
    childViewContainer: 'ul'
    childViewOptions: (m) ->
      id: m.id

    ui:
      button: 'send'
      input: 'textarea'

    events:
      'click button': 'send'

    send: ->
      window.socket.send JSON.stringify message: @ui.input.val()
      @ui.input.val('')

  class Chat.Router extends Marionette.AppRouter
    controller:
      chat: () ->
        window.dialog = new Chat.Dialog()
        App.main.show new Chat.DialogView
          collection: window.dialog

    appRoute:
      chat: 'chat'

window.App = App
window.onload = ->
  router = new App.Chat.Router()
  router.controller.chat()
  App.main.currentView.collection.fetch()

create_chat = ->
  window.socket = new WebSocket 'ws://localhost:5000/'
  window.socket.addEventListener 'open', chat_open

window.auth = Date.now().toString(32)
chat_open = ->
#  if $.cookie 'auth'
#    $.cookie 'auth', Date.now().toString(32), {expire: 30, path: '/'}
  @send JSON.stringify
    command: 'enter'
    auth: window.auth

  @addEventListener 'message', (e) ->
    window.dialog.add JSON.parse(e.data)

  @addEventListener 'close', () ->
    setTimeout create_chat, 5000

create_chat()
